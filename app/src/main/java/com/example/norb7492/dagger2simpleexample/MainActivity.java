package com.example.norb7492.dagger2simpleexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;


import com.example.norb7492.dagger2simpleexample.dagger2.DaggerPessoaComponent;
import com.example.norb7492.dagger2simpleexample.dagger2.PessoaComponent;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {



    //Instanciar o componente
    PessoaComponent pessoaComponent;


    //Não precisa do keyword new para acessar o PessoaObject
    @Inject
    PessoaObject pessoaObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //Instanciar o dagger 2
        pessoaComponent = DaggerPessoaComponent.builder()
                .build();
        pessoaComponent.injetarModuloPessoaNaActivityMain(MainActivity.this);//passar o contexto para o componente



        //Pode setar as coisas
        pessoaObject.setNome("Gorick no dagger2");


        Log.d("TesteDagger2", pessoaObject.getNome());



    }
}
