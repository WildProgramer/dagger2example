package com.example.norb7492.dagger2simpleexample.dagger2;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Norb7492 on 03/03/2017.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface PerActivity {
}
