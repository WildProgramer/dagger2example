package com.example.norb7492.dagger2simpleexample;

/**
 * Created by Norb7492 on 03/03/2017.
 */

public class PessoaObject {

    private String nome;

    public PessoaObject(String nome) {
        this.nome = nome;
    }

    public PessoaObject() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
