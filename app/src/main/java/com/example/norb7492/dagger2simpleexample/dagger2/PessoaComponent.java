package com.example.norb7492.dagger2simpleexample.dagger2;

import com.example.norb7492.dagger2simpleexample.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Norb7492 on 03/03/2017.
 */

@PerActivity
@Component(modules = {PessoaModule.class})
public interface PessoaComponent {



    //Este metodo vai se instanciado onde o modulo vai se instanciado
    void injetarModuloPessoaNaActivityMain(MainActivity mainActivity);

}
