package com.example.norb7492.dagger2simpleexample.dagger2;



import com.example.norb7492.dagger2simpleexample.PessoaObject;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Norb7492 on 03/03/2017.
 */

@Module
public class PessoaModule {


    //Estou instanciando new PessoaModule

    @PerActivity
    @Provides
    PessoaObject instanciarPessoa(){

        return new PessoaObject();

    }
}
